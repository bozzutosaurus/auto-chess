import typing

from pieces.piece import Piece
from position import Position


class ChessBoard:

    def __init__(self):
        self.ranks = 8
        self.files = 8

        self.min_rank = 1
        self.max_rank = 8
        self.min_file = 'a'
        self.max_file = 'h'

        self._raw_board: typing.List[typing.List[typing.Optional[Piece]]] = [
            [None for _ in range(self.files)] for _ in range(self.ranks)
        ]

    def _get_piece_at_indicies(self, indicies: typing.Tuple[int, int]) -> typing.Optional[Piece]:
        return self._raw_board[indicies[0]][indicies[1]]

    def _set_piece_at_indicies(self, piece: typing.Optional[Piece], indicies: typing.Tuple[int, int]):
        self._raw_board[indicies[0]][indicies[1]] = piece

    @staticmethod
    def _rank_to_index(rank: int):
        return 8 - rank

    @staticmethod
    def _file_to_index(file: str):
        return ord(file) - ord('a')

    def _position_to_file_rank_indicies(self, position: Position) -> typing.Tuple[int, int]:
        return self._rank_to_index(position.rank), self._file_to_index(position.file),

    def get_piece_at_position(self, position: Position) -> typing.Optional[Piece]:
        return self._get_piece_at_indicies(self._position_to_file_rank_indicies(position))

    def place_piece_at_position(self, piece: Piece, position: Position):
        self._set_piece_at_indicies(piece, self._position_to_file_rank_indicies(position))
        piece.position = position

    def remove_piece_at_position(self, position: Position):
        self._set_piece_at_indicies(None, self._position_to_file_rank_indicies(position))

    def move_piece_at_position_to_position(self, source: Position, destination: Position):
        piece = self.get_piece_at_position(source)
        if not piece:
            raise IndexError(f'No piece at position (was given {source})')

        self.remove_piece_at_position(source)
        self.place_piece_at_position(piece, destination)

    def print(self):
        board_row = '+-------+-------+-------+-------+-------+-------+-------+-------+\n'
        board_mid = '|       |       |       |       |       |       |       |       |\n'
        board_render = board_row
        for rank in range(self.files):
            board_render += board_mid
            board_render += '|'
            for file in range(self.ranks):
                piece = self._raw_board[rank][file]
                board_render += f"  {piece.symbol if piece else '  '}   |"
            board_render += '\n'
            board_render += board_mid
            board_render += board_row

        print(board_render)
