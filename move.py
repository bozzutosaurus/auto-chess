from pieces import Piece
from position import Position


class Move:

    def __init__(self, piece: Piece, source: Position, destination: Position):
        self.piece = piece
        self.source = source
        self.destination = destination
