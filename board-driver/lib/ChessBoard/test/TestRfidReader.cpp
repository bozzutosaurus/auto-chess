#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "RfidReader.h"
#include "../../MFRC522/src/MockMFRC522.h"

class RfidReaderTest : public testing::Test {
};

void ExpectSuccessfulStartup(MockMFRC522 * mockMfrc522)
{
    EXPECT_CALL(*mockMfrc522, begin()).Times(1);
    EXPECT_CALL(*mockMfrc522, getFirmwareVersion()).WillOnce(testing::Return(0x01));
}

TEST(RfidReaderTest, StartupFailsIfFirmwareVersionCannotBeRead)
{
    MockMFRC522 mockMfrc522;
    EXPECT_CALL(mockMfrc522, begin()).Times(1);
    EXPECT_CALL(mockMfrc522, getFirmwareVersion()).WillOnce(testing::Return(0));

    RfidReader reader(&mockMfrc522);
    EXPECT_FALSE(reader.Startup());
}

TEST(RfidReaderTest, StartupSucceeds)
{
    MockMFRC522 mockMfrc522;
    ExpectSuccessfulStartup(&mockMfrc522);

    RfidReader reader(&mockMfrc522);
    EXPECT_TRUE(reader.Startup());
}

TEST(RfidReaderTest, FailureToGetTagReturnsNullptr)
{
    MockMFRC522 mockMfrc522;
    ExpectSuccessfulStartup(&mockMfrc522);
    EXPECT_CALL(mockMfrc522, requestTag(MF1_REQIDL, testing::_)).WillOnce(testing::Return(MI_ERR));

    RfidReader reader(&mockMfrc522);
    EXPECT_TRUE(reader.Startup());
    EXPECT_EQ(nullptr, reader.GetTag());
}

TEST(RfidReaderTest, SerialNumberReturnedInTag)
{
    MockMFRC522 mockMfrc522;
    ExpectSuccessfulStartup(&mockMfrc522);
    uint8_t tagInfo[] = {0xDE, 0xAD, 0xBE, 0xEF, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    EXPECT_CALL(mockMfrc522, requestTag(MF1_REQIDL, testing::NotNull()))
            .WillOnce(testing::DoAll(testing::SetArrayArgument<1>(tagInfo, tagInfo+16),
                                     testing::Return(MI_OK)));
    RfidReader reader(&mockMfrc522);
    EXPECT_TRUE(reader.Startup());

    auto tag = reader.GetTag();
    EXPECT_EQ(0xDEADBEEF, tag->GetSerialNumber());
    delete tag;
}

