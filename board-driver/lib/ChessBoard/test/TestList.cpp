#include "gtest/gtest.h"
#include "../include/List.h"

class ListTest : public testing::Test {
};

TEST(ListTest, ListIsInitiallyEmpty) {
    LinkedList<int> myList;

    EXPECT_EQ(0, myList.size());
    EXPECT_TRUE(myList.empty());
}

TEST(ListTest, AddSingleElementAtFront) {
    LinkedList<int> myList;
    myList.insertFront(10);

    EXPECT_EQ(1, myList.size());
    EXPECT_FALSE(myList.empty());
    EXPECT_EQ(10, *myList.at(0));
}

TEST(ListTest, AddSingleElementAtBack) {
    LinkedList<int> myList;
    myList.insertBack(10);

    EXPECT_EQ(1, myList.size());
    EXPECT_FALSE(myList.empty());
    EXPECT_EQ(10, *myList.at(0));
}

TEST(ListTest, AddMultipleElementsAtFront)
{
    LinkedList<int> myList;
    myList.insertFront(1);
    myList.insertFront(2);
    myList.insertFront(3);

    EXPECT_EQ(3, myList.size());
    EXPECT_EQ(3, *myList.at(0));
    EXPECT_EQ(2, *myList.at(1));
    EXPECT_EQ(1, *myList.at(2));
}

TEST(ListTest, AddMultipleElementsAtBack)
{
    LinkedList<int> myList;
    myList.insertBack(1);
    myList.insertBack(2);
    myList.insertBack(3);

    EXPECT_EQ(3, myList.size());
    EXPECT_EQ(1, *myList.at(0));
    EXPECT_EQ(2, *myList.at(1));
    EXPECT_EQ(3, *myList.at(2));
}

TEST(ListTest, AddEntryAtMiddle)
{
    LinkedList<int> myList;
    myList.insertBack(1);
    myList.insertBack(3);
    myList.insertAt(1, 2);

    EXPECT_EQ(3, myList.size());
    EXPECT_EQ(1, *myList.at(0));
    EXPECT_EQ(2, *myList.at(1));
    EXPECT_EQ(3, *myList.at(2));
}

TEST(ListTest, AddEntryAtBadIndexDefaultsToEnd)
{
    LinkedList<int> myList;
    myList.insertBack(1);
    myList.insertAt(1000, 2);

    EXPECT_EQ(2, myList.size());
    EXPECT_EQ(1, *myList.at(0));
    EXPECT_EQ(2, *myList.at(1));
}

TEST(ListTest, RemoveSingleEntry)
{
    LinkedList<int> myList;
    myList.insertFront(1);
    myList.removeAt(0);

    EXPECT_EQ(myList.size(), 0);
    EXPECT_TRUE(myList.empty());
}

TEST(ListTest, RemoveFrontEntry)
{
    LinkedList<int> myList;
    myList.insertBack(1);
    myList.insertBack(2);
    myList.insertBack(3);
    myList.removeAt(0);

    EXPECT_EQ(2, myList.size());
    EXPECT_EQ(2, *myList.at(0));
    EXPECT_EQ(3, *myList.at(1));
}

TEST(ListTest, RemoveRearEntry)
{
    LinkedList<int> myList;
    myList.insertBack(1);
    myList.insertBack(2);
    myList.insertBack(3);
    myList.removeAt(2);

    EXPECT_EQ(2, myList.size());
    EXPECT_EQ(1, *myList.at(0));
    EXPECT_EQ(2, *myList.at(1));
}

TEST(ListTest, RemoveMiddleEntry)
{
    LinkedList<int> myList;
    myList.insertBack(1);
    myList.insertBack(2);
    myList.insertBack(3);
    myList.removeAt(1);

    EXPECT_EQ(2, myList.size());
    EXPECT_EQ(1, *myList.at(0));
    EXPECT_EQ(3, *myList.at(1));
}

TEST(ListTest, RemoveAtInvalidIndexDoesNotAffectSize)
{
    LinkedList<int> myList;
    myList.insertBack(1);
    myList.removeAt(1000);

    EXPECT_EQ(1, myList.size());
    EXPECT_EQ(1, *myList.at(0));
}

TEST(ListTest, AccessingElementAtInvalidIndexReturnsNullptr)
{
    LinkedList<int> myList;
    myList.insertBack(1);

    EXPECT_EQ(1, myList.size());
    EXPECT_EQ(1, myList.size());
    EXPECT_EQ(nullptr, myList.at(1));
}

TEST(ListTest, FirstLastAliases)
{
    LinkedList<int> myList;
    myList.insertBack(1);
    myList.insertBack(2);
    myList.insertBack(3);

    EXPECT_EQ(1, *myList.atFront());
    EXPECT_EQ(3, *myList.atBack());

    myList.removeFirst();
    EXPECT_EQ(2, *myList.atFront());

    myList.removeLast();
    EXPECT_EQ(2, *myList.atBack());
}
