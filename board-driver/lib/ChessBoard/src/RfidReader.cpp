#include "../include/RfidReader.h"

RfidReader::RfidReader(IMFRC522 *mfrc522) : m_Mfrc522(mfrc522) {}

bool RfidReader::Startup() {
    m_Mfrc522->begin();
    unsigned char firmwareVersion = m_Mfrc522->getFirmwareVersion();
    if (!firmwareVersion) {
        return false;
    }
    return true;
}

RfidTag * RfidReader::GetTag() {
    auto tag = new RfidTag();
    int status = m_Mfrc522->requestTag(MF1_REQIDL, tag->data);
    if (status != MI_OK) {
        delete tag;
        return nullptr;
    }

    return tag;
}


