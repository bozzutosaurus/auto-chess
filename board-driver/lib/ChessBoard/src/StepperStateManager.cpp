#include "../include/StepperStateManager.h"

StepperStateManager::StepperStateManager(unsigned int frequency) :
    m_Frequency(frequency) {
}

void StepperStateManager::addMotor(StepperMotor * motor) {
    m_Motors.insertBack(motor);
}
