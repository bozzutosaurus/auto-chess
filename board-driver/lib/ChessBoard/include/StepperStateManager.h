#pragma once

#include "StepperMotor.h"

class StepperStateManager {
public:
    StepperStateManager(unsigned int frequency);

    void addMotor(StepperMotor * motor);

private:
    unsigned int m_Frequency;
    LinkedList<StepperMotor*> m_Motors;
};
