#pragma once

template <class T> class LinkedListNode {
public:
    LinkedListNode(T data) : m_Data(data), m_Next(nullptr) {}

//private:
    T m_Data;
    LinkedListNode<T>* m_Next;
};

template <class T> class LinkedList {
public:
    LinkedList<T>() : m_Head(nullptr), m_Size(0) {}

    bool empty() {
        return m_Head == nullptr;
    }

    unsigned int size() {
        return m_Size;
    }

    void insertAt(unsigned int index, T entry) {
        if (index > m_Size) {
            index = m_Size;  // Treat too-big indicies as the end of list
        }

        unsigned int currentIndex = 0;
        LinkedListNode<T>* lastEntry = nullptr;
        auto currentEntry = m_Head;
        while (currentEntry != nullptr && currentIndex != index) {
            lastEntry = currentEntry;
            currentEntry = currentEntry->m_Next;
            if (++currentIndex == index) {
                break;
            }
        }

        if (lastEntry == nullptr) { // Beginning of List
            auto newEntry = new LinkedListNode<T>(entry);
            newEntry->m_Next = m_Head;
            m_Head = newEntry;
            m_Size++;
        }

        else if (currentEntry == nullptr) { // End of List
            lastEntry->m_Next = new LinkedListNode<T>(entry);
            m_Size++;
        }

        else { // Middle of List
            auto newEntry = new LinkedListNode<T>(entry);
            newEntry->m_Next = lastEntry->m_Next;
            lastEntry->m_Next = newEntry;
            m_Size++;
        }
    }

    void insertFront(T entry) {
        insertAt(0, entry);
    }

    void insertBack(T entry) {
        if (empty()) {
            m_Head = new LinkedListNode<T>(entry);
            m_Size++;
            return;
        }

        auto lastEntry = m_Head;
        while (lastEntry->m_Next) {
            lastEntry = lastEntry->m_Next;
        }
        lastEntry->m_Next = new LinkedListNode<T>(entry);
        m_Size++;
    }

    void removeAt(unsigned int index) {
        if (index >= m_Size) {
            return;
        }

        LinkedListNode<T>* lastEntry = nullptr;
        auto currentEntry = m_Head;
        while (index > 0) {
            lastEntry = currentEntry;
            currentEntry = currentEntry->m_Next;
            index--;
        }

        if (lastEntry == nullptr) { // First element
            LinkedListNode<T>* oldHead = m_Head;
            m_Head = m_Head->m_Next;
            delete oldHead;
            m_Size--;
        }
        else {
            delete currentEntry;
            lastEntry->m_Next = currentEntry->m_Next;
            m_Size--;
        }
    }

    void removeFirst() {
        removeAt(0);
    }

    void removeLast() {
        removeAt(m_Size - 1);
    }

    T* at(unsigned int index) {
        if (index >= m_Size) {
            return nullptr;
        }

        auto currentEntry = m_Head;
        while (index-- > 0) {
            currentEntry = currentEntry->m_Next;
        }

        return &(currentEntry->m_Data);
    }

    T* atFront() {
        return at(0);
    }

    T* atBack() {
        return at(m_Size - 1);
    }

private:
    LinkedListNode<T>* m_Head;
    unsigned int m_Size;
};

