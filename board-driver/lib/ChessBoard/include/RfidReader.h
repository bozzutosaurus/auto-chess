#pragma once
#include <stdlib.h>
#include <stdint.h>
#include "../../MFRC522/src/MFRC522.h"

struct RfidTag {
    uint8_t data[16];
    uint32_t GetSerialNumber() {
        return (((uint32_t) data[0]) << 24) +
               (((uint32_t) data[1]) << 16) +
               (((uint32_t) data[2]) << 8) +
               ((uint32_t) data[3]);

    }
};

class RfidReader {
public:
    RfidReader(IMFRC522 * mfrc522);
    bool Startup();
    RfidTag* GetTag();

private:
    IMFRC522 * m_Mfrc522;
};
