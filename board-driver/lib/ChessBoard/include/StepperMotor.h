#pragma once

#include "List.h"

class StepperMotor {
public:
    StepperMotor(unsigned int stepsPerRevolution);

private:
    unsigned int m_StepsPerRevolution;

};
