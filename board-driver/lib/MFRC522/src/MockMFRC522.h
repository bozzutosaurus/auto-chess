#pragma once
#include "gmock/gmock.h"
#include "MFRC522.h"

class MockMFRC522 : public IMFRC522 {
public:
    MockMFRC522() : IMFRC522() {}
    MOCK_METHOD(void, writeToRegister, (byte addr, byte val), (override));
    MOCK_METHOD(byte, readFromRegister, (byte addr), (override));
    MOCK_METHOD(void, setBitMask, (byte addr, byte mask), (override));
    MOCK_METHOD(void, clearBitMask, (byte addr, byte mask), (override));
    MOCK_METHOD(void, begin, (), (override));
    MOCK_METHOD(void, reset, (), (override));
    MOCK_METHOD(byte, getFirmwareVersion, (), (override));
    MOCK_METHOD(boolean, digitalSelfTestPass, (), (override));
    MOCK_METHOD(int, commandTag, (byte command, byte *data, int dlen, byte *result, int *rlen), (override));
    MOCK_METHOD(int, requestTag, (byte mode, byte *type), (override));
    MOCK_METHOD(int, antiCollision, (byte *serial), (override));
    MOCK_METHOD(void, calculateCRC, (byte *data, int len, byte *result), (override));
    MOCK_METHOD(byte, selectTag, (byte *serial), (override));
    MOCK_METHOD(int, authenticate, (byte mode, byte block, byte *key, byte *serial), (override));
    MOCK_METHOD(int, readFromTag, (byte blockAddr, byte *recvData), (override));
    MOCK_METHOD(int, writeToTag, (byte blockAddr, byte *writeData), (override));
    MOCK_METHOD(int, haltTag, (), (override));
};
