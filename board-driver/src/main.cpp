#include <Arduino.h>
#include <List.h>
#include <RfidReader.h>
#include <StepperMotor.h>
#include <StepperStateManager.h>

#define CHECK(value) if (value) { return; }

void setup() {
    Serial.begin(9600);
    Serial.println("Starting setup");
    auto stepperStateManager = StepperStateManager(100);
    Serial.println("Made manager");

    auto xAxis = StepperMotor(200);
    Serial.println("Made x motor");

    auto yAxis = StepperMotor(200);
    Serial.println("Made y moitor");
    stepperStateManager.addMotor(&xAxis);
    Serial.println("Added x motor");
    stepperStateManager.addMotor(&yAxis);
    Serial.println("Added y motor");
    Serial.println("Finished setup");

}

void loop() {
    Serial.println("Starting Loop");

    delay(10000);

// write your code here
}
