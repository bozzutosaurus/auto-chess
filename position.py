import typing


class Position:

    def __init__(self, *args, rank: int = None, file: str = None, position: str = None):
        """
        The position can be denoted explicitly by rank AND file, or by a single position string via keyword args
        Optionally, a single arg can be passed and a best attempt determination will be done. Using args takes priority
        over the keyword args.
        """
        if args:
            if type(args[0]) == tuple and len(args[0]) == 2:
                if type(args[0][0]) == int and type(args[0][1]) == str:
                    rank, file = args[0][0], args[0][1]
                elif type(args[0][0]) == str and type(args[0][1] == int):
                    file, rank = args[0][0], args[0][1]

            elif type(args[0]) == str and len(args[0]) == 2:
                position = args[0]
            else:
                raise InvalidPositionError(f'Invalid arguments supplied to position: {args}')

        if rank and file:
            self.validate_rank(rank)
            self.validate_file(file)

            self.rank = rank
            self.file = file.lower()
            self.position = self._rank_and_file_to_position_str(file, rank)

        elif position:
            self.validate_position(position)

            self.position = position
            self.file, self.rank = self._position_str_to_rank_and_file(position)

        else:
            raise InvalidPositionError('Position must have either rank and file or a position')

    def __eq__(self, other: 'Position') -> bool:
        return isinstance(other, Position) and self.rank == other.rank and self.file == other.file

    def __ne__(self, other: 'Position'):
        return self != other

    def __str__(self) -> str:
        return self.position

    def __repr__(self) -> str:
        return str(self)

    def __hash__(self):
        return hash(self.rank) ^ hash(self.file)

    @staticmethod
    def make(*args, rank: int = None, file: str = None, position: str = None) -> typing.Optional['Position']:
        """
        Provides a factory function for positions that can optionally return None if the position is invalid
        """
        try:
            return Position(*args, rank=rank, file=file, position=position)
        except InvalidPositionError:
            return None

    @staticmethod
    def validate_rank(rank: int):
        if not (1 <= rank <= 8):
            raise InvalidPositionError(f'Rank must be between 1 and 8 (was given {rank})')

    @staticmethod
    def validate_file(file: str):
        file = file.lower()
        if file not in 'abcdefgh':
            raise InvalidPositionError(f'File must be between A and H (was given {file}')

    @staticmethod
    def validate_position(position: str):
        if len(position) != 2:
            raise InvalidPositionError(f'Invalid position (was given {position})')

    @staticmethod
    def _rank_and_file_to_position_str(file: str, rank: int) -> str:
        Position.validate_file(file)
        Position.validate_rank(rank)

        return f'{file}{rank}'.lower()

    @staticmethod
    def _position_str_to_rank_and_file(position: str) -> typing.Tuple[str, int]:
        Position.validate_position(position)
        return position[0].lower(), int(position[1])


class InvalidPositionError(Exception):
    pass
