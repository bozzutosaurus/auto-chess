from board import ChessBoard
from pieces import PieceColor, King, Pawn
from position import Position


class TestKing:

    def test_symbol(self):
        assert King(PieceColor.WHITE).symbol == 'wK'

    def test_open_king_can_move_one_in_all_directions(self, board: ChessBoard):
        king = King(PieceColor.WHITE)
        board.place_piece_at_position(king, Position('e4'))
        assert king.available_positions_on_board(board) == {
            Position('d3'), Position('d4'), Position('d5'),
            Position('e3'), Position('e5'),
            Position('f3'), Position('f4'), Position('f5')
        }

    def test_boxed_in_king_has_no_moves(self, board: ChessBoard):
        king = King(PieceColor.WHITE)
        board.place_piece_at_position(king, Position('e4'))
        board.place_piece_at_position(Pawn(PieceColor.WHITE), Position('d3'))
        board.place_piece_at_position(Pawn(PieceColor.WHITE), Position('d4'))
        board.place_piece_at_position(Pawn(PieceColor.WHITE), Position('d5'))
        board.place_piece_at_position(Pawn(PieceColor.WHITE), Position('f3'))
        board.place_piece_at_position(Pawn(PieceColor.WHITE), Position('f4'))
        board.place_piece_at_position(Pawn(PieceColor.WHITE), Position('f5'))
        board.place_piece_at_position(Pawn(PieceColor.WHITE), Position('e3'))
        board.place_piece_at_position(Pawn(PieceColor.WHITE), Position('e5'))

        assert not king.available_positions_on_board(board)

    def test_king_can_capture_pieces(self, board: ChessBoard):
        king = King(PieceColor.WHITE)
        board.place_piece_at_position(king, Position('a1'))
        board.place_piece_at_position(Pawn(PieceColor.BLACK), Position('a2'))
        board.place_piece_at_position(Pawn(PieceColor.BLACK), Position('b1'))
        board.place_piece_at_position(Pawn(PieceColor.BLACK), Position('b2'))

        assert king.available_positions_on_board(board) == {Position('a2'), Position('b1'), Position('b2')}
