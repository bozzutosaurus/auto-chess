from board import ChessBoard
from pieces import PieceColor, Bishop
from position import Position


class TestBishop:

    def test_symbol(self):
        assert Bishop(PieceColor.WHITE).symbol == 'wB'

    def test_bishop_with_open_board_has_all_diagonal_squares(self, board: ChessBoard):
        bishop = Bishop(PieceColor.WHITE)
        board.place_piece_at_position(bishop, Position('e4'))
        assert bishop.available_positions_on_board(board) == {
            Position('d5'), Position('c6'), Position('b7'), Position('a8'),
            Position('f3'), Position('g2'), Position('h1'),
            Position('b1'), Position('c2'), Position('d3'),
            Position('f5'), Position('g6'), Position('h7')
        }

    def test_boxed_in_bishop_has_no_moves(self, board: ChessBoard):
        bishop = Bishop(PieceColor.WHITE)
        board.place_piece_at_position(bishop, Position('e4'))
        board.place_piece_at_position(Bishop(PieceColor.WHITE), Position('d3'))
        board.place_piece_at_position(Bishop(PieceColor.WHITE), Position('d5'))
        board.place_piece_at_position(Bishop(PieceColor.WHITE), Position('f3'))
        board.place_piece_at_position(Bishop(PieceColor.WHITE), Position('f5'))

        assert not bishop.available_positions_on_board(board)

    def test_bishop_moves_stop_after_encountering_enemy_piece(self, board: ChessBoard):
        bishop = Bishop(PieceColor.WHITE)
        board.place_piece_at_position(bishop, Position('a1'))
        board.place_piece_at_position(Bishop(PieceColor.BLACK), Position('d4'))

        assert bishop.available_positions_on_board(board) == {Position('b2'), Position('c3'), Position('d4')}
