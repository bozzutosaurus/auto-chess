from board import ChessBoard
from pieces import PieceColor, Pawn
from position import Position


class TestPawn:

    def test_symbol(self):
        assert Pawn(PieceColor.WHITE).symbol == 'wP'

    def test_pawns_in_initial_position_can_move_two_squares(self, board: ChessBoard):
        white_pawn = Pawn(PieceColor.WHITE)
        board.place_piece_at_position(white_pawn, Position('e2'))
        assert white_pawn.available_positions_on_board(board) == {Position('e3'), Position('e4')}

        black_pawn = Pawn(PieceColor.BLACK)
        board.place_piece_at_position(black_pawn, Position('e7'))
        assert black_pawn.available_positions_on_board(board) == {Position('e6'), Position('e5')}

    def test_pawns_cannot_move_two_squares_after_moving(self, board: ChessBoard):
        white_pawn = Pawn(PieceColor.WHITE)
        board.place_piece_at_position(white_pawn, Position('a3'))
        assert white_pawn.available_positions_on_board(board) == {Position('a4')}

        black_pawn = Pawn(PieceColor.BLACK)
        board.place_piece_at_position(black_pawn, Position('g6'))
        assert black_pawn.available_positions_on_board(board) == {Position('g5')}

    def test_same_colored_doubled_pawn_has_no_moves(self, board: ChessBoard):
        rear_white_pawn = Pawn(PieceColor.WHITE)
        board.place_piece_at_position(rear_white_pawn, Position('c2'))
        forward_white_pawn = Pawn(PieceColor.WHITE)
        board.place_piece_at_position(forward_white_pawn, Position('c3'))
        assert not rear_white_pawn.available_positions_on_board(board)

        rear_black_pawn = Pawn(PieceColor.BLACK)
        board.place_piece_at_position(rear_black_pawn, Position('c7'))
        forward_black_pawn = Pawn(PieceColor.BLACK)
        board.place_piece_at_position(forward_black_pawn, Position('c6'))
        assert not rear_black_pawn.available_positions_on_board(board)

    def test_pawn_cannot_capture_own_pawn(self, board: ChessBoard):
        rear_white_pawn = Pawn(PieceColor.WHITE)
        board.place_piece_at_position(rear_white_pawn, Position('g5'))
        forward_white_pawn = Pawn(PieceColor.WHITE)
        board.place_piece_at_position(forward_white_pawn, Position('h6'))
        assert rear_white_pawn.available_positions_on_board(board) == {Position('g6')}

        rear_black_pawn = Pawn(PieceColor.BLACK)
        board.place_piece_at_position(rear_black_pawn, Position('a4'))
        forward_black_pawn = Pawn(PieceColor.BLACK)
        board.place_piece_at_position(forward_black_pawn, Position('b3'))
        assert rear_black_pawn.available_positions_on_board(board) == {Position('a3')}

    def test_pawn_blocked_by_enemy_piece(self, board: ChessBoard):
        white_pawn = Pawn(PieceColor.WHITE)
        board.place_piece_at_position(white_pawn, Position('e4'))
        black_pawn = Pawn(PieceColor.BLACK)
        board.place_piece_at_position(black_pawn, Position('e5'))

        assert not white_pawn.available_positions_on_board(board)
        assert not black_pawn.available_positions_on_board(board)

    def test_pawn_can_single_capture_enemy_piece(self, board: ChessBoard):
        white_pawn = Pawn(PieceColor.WHITE)
        board.place_piece_at_position(white_pawn, Position('e4'))
        black_pawn = Pawn(PieceColor.BLACK)
        board.place_piece_at_position(black_pawn, Position('d5'))

        assert white_pawn.available_positions_on_board(board) == {Position('e5'), Position('d5')}
        assert black_pawn.available_positions_on_board(board) == {Position('d4'), Position('e4')}

    def test_pawn_can_fork_enemy_pieces(self, board: ChessBoard):
        left_white_pawn = Pawn(PieceColor.WHITE)
        right_white_pawn = Pawn(PieceColor.WHITE)
        left_black_pawn = Pawn(PieceColor.BLACK)
        right_black_pawn = Pawn(PieceColor.BLACK)

        board.place_piece_at_position(left_white_pawn, Position('c4'))
        board.place_piece_at_position(right_white_pawn, Position('e4'))
        board.place_piece_at_position(left_black_pawn, Position('d5'))
        board.place_piece_at_position(right_black_pawn, Position('f5'))

        assert right_white_pawn.available_positions_on_board(board) == {Position('d5'), Position('e5'), Position('f5')}
        assert left_black_pawn.available_positions_on_board(board) == {Position('c4'), Position('d4'), Position('e4')}

    # TODO: Support for en-passant
