from board import ChessBoard
from pieces import PieceColor, Rook
from position import Position


class TestRook:

    def test_symbol(self):
        assert Rook(PieceColor.WHITE).symbol == 'wR'

    def test_bishop_with_open_board_has_all_diagonal_squares(self, board: ChessBoard):
        rook = Rook(PieceColor.WHITE)
        board.place_piece_at_position(rook, Position('e4'))
        assert rook.available_positions_on_board(board) == {
            Position('a4'), Position('b4'), Position('c4'), Position('d4'),
            Position('f4'), Position('g4'), Position('h4'),
            Position('e1'), Position('e2'), Position('e3'),
            Position('e5'), Position('e6'), Position('e7'), Position('e8')
        }

    def test_boxed_in_bishop_has_no_moves(self, board: ChessBoard):
        rook = Rook(PieceColor.WHITE)
        board.place_piece_at_position(rook, Position('e4'))
        board.place_piece_at_position(Rook(PieceColor.WHITE), Position('e3'))
        board.place_piece_at_position(Rook(PieceColor.WHITE), Position('e5'))
        board.place_piece_at_position(Rook(PieceColor.WHITE), Position('d4'))
        board.place_piece_at_position(Rook(PieceColor.WHITE), Position('f4'))

        assert not rook.available_positions_on_board(board)

    def test_bishop_moves_stop_after_encountering_enemy_piece(self, board: ChessBoard):
        rook = Rook(PieceColor.WHITE)
        board.place_piece_at_position(rook, Position('a1'))
        board.place_piece_at_position(Rook(PieceColor.BLACK), Position('a3'))
        board.place_piece_at_position(Rook(PieceColor.BLACK), Position('b1'))

        assert rook.available_positions_on_board(board) == {Position('a2'), Position('a3'), Position('b1')}
