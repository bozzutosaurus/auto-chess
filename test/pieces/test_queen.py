from board import ChessBoard
from pieces import PieceColor, Queen
from position import Position


class TestQueen:

    def test_symbol(self):
        assert Queen(PieceColor.WHITE).symbol == 'wQ'

    def test_queen_with_open_board_has_all_squares(self, board: ChessBoard):
        queen = Queen(PieceColor.WHITE)
        board.place_piece_at_position(queen, Position('e4'))
        assert queen.available_positions_on_board(board) == {
            Position('d5'), Position('c6'), Position('b7'), Position('a8'),
            Position('f3'), Position('g2'), Position('h1'),
            Position('b1'), Position('c2'), Position('d3'),
            Position('f5'), Position('g6'), Position('h7'),
            Position('a4'), Position('b4'), Position('c4'), Position('d4'),
            Position('f4'), Position('g4'), Position('h4'),
            Position('e1'), Position('e2'), Position('e3'),
            Position('e5'), Position('e6'), Position('e7'), Position('e8')
        }

    def test_boxed_in_queen_has_no_moves(self, board: ChessBoard):
        queen = Queen(PieceColor.WHITE)
        board.place_piece_at_position(queen, Position('e4'))
        board.place_piece_at_position(Queen(PieceColor.WHITE), Position('d3'))
        board.place_piece_at_position(Queen(PieceColor.WHITE), Position('d4'))
        board.place_piece_at_position(Queen(PieceColor.WHITE), Position('d5'))
        board.place_piece_at_position(Queen(PieceColor.WHITE), Position('f3'))
        board.place_piece_at_position(Queen(PieceColor.WHITE), Position('f4'))
        board.place_piece_at_position(Queen(PieceColor.WHITE), Position('f5'))
        board.place_piece_at_position(Queen(PieceColor.WHITE), Position('e3'))
        board.place_piece_at_position(Queen(PieceColor.WHITE), Position('e5'))

        assert not queen.available_positions_on_board(board)

    def test_queen_moves_stop_after_encountering_enemy_piece(self, board: ChessBoard):
        queen = Queen(PieceColor.WHITE)
        board.place_piece_at_position(queen, Position('a1'))
        board.place_piece_at_position(Queen(PieceColor.BLACK), Position('d4'))
        board.place_piece_at_position(Queen(PieceColor.BLACK), Position('a2'))
        board.place_piece_at_position(Queen(PieceColor.BLACK), Position('b1'))

        assert queen.available_positions_on_board(board) == {Position('b2'), Position('c3'), Position('d4'),
                                                             Position('a2'), Position('b1')}
