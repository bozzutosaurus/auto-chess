from board import ChessBoard
from pieces import PieceColor, Knight
from position import Position


class TestKnight:

    def test_symbol(self):
        assert Knight(PieceColor.WHITE).symbol == 'wN'

    def test_knight_in_middle_of_board_has_eight_squares(self, board: ChessBoard):
        knight = Knight(PieceColor.WHITE)
        board.place_piece_at_position(knight, Position('e4'))
        assert knight.available_positions_on_board(board) == {
            Position('d6'), Position('f6'), Position('c5'), Position('g5'),
            Position('c3'), Position('g3'), Position('d2'), Position('f2')
        }

    def test_knight_on_edge_of_board_doesnt_have_all_squares(self, board: ChessBoard):
        knight = Knight(PieceColor.WHITE)
        board.place_piece_at_position(knight, Position('a7'))
        assert knight.available_positions_on_board(board) == {Position('c8'), Position('c6'), Position('b5')}

    def test_knight_cannot_move_to_own_pieces(self, board: ChessBoard):
        knight = Knight(PieceColor.WHITE)
        board.place_piece_at_position(knight, Position('h8'))
        board.place_piece_at_position(Knight(PieceColor.WHITE), Position('f7'))
        board.place_piece_at_position(Knight(PieceColor.WHITE), Position('g6'))
        assert not knight.available_positions_on_board(board)

    def test_knight_can_capture_enemy_pieces(self, board: ChessBoard):
        knight = Knight(PieceColor.WHITE)
        board.place_piece_at_position(knight, Position('a1'))
        board.place_piece_at_position(Knight(PieceColor.BLACK), Position('b3'))
        board.place_piece_at_position(Knight(PieceColor.WHITE), Position('c2'))
        assert knight.available_positions_on_board(board) == {Position('b3')}

