import pytest
from board import *
from pieces.pawn import Pawn
from pieces.piece import PieceColor
from position import InvalidPositionError


class TestBoard:

    def test_place_piece_at_invalid_file_fails(self, board: ChessBoard):
        with pytest.raises(InvalidPositionError):
            board.place_piece_at_position(Pawn(PieceColor.WHITE), Position(file='k', rank=2))

    def test_place_piece_at_invalid_rank_fails(self, board: ChessBoard):
        with pytest.raises(InvalidPositionError):
            board.place_piece_at_position(Pawn(PieceColor.WHITE), Position(file='e', rank=0))

    def test_place_piece_at_invalid_position_fails(self, board: ChessBoard):
        with pytest.raises(InvalidPositionError):
            board.place_piece_at_position(Pawn(PieceColor.WHITE), Position(position='blah'))

    def test_successful_placements(self, board: ChessBoard):
        piece = Pawn(PieceColor.WHITE)
        board.place_piece_at_position(piece, Position(file='a', rank=2))
        assert piece == board.get_piece_at_position(Position(position='a2'))

        piece2 = Pawn(PieceColor.WHITE)
        board.place_piece_at_position(piece2, Position(position='a3'))
        assert piece2 == board.get_piece_at_position(Position(position='a3'))
