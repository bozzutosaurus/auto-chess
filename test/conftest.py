import pytest
from board import ChessBoard


@pytest.fixture(scope='function')
def board() -> ChessBoard:
    return ChessBoard()
