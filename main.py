from board import ChessBoard
from pieces.bishop import Bishop
from pieces.king import King
from pieces.knight import Knight
from pieces.pawn import Pawn
from pieces.piece import PieceColor
from pieces.queen import Queen
from pieces.rook import Rook
from position import Position


def create_board() -> ChessBoard:

    def place_pieces_in_rank(_board: ChessBoard, rank: int, owner: PieceColor):
        _board.place_piece_at_position(Rook(owner), Position(file='a', rank=rank))
        _board.place_piece_at_position(Knight(owner), Position(file='b', rank=rank))
        _board.place_piece_at_position(Bishop(owner), Position(file='c', rank=rank))
        _board.place_piece_at_position(Queen(owner), Position(file='d', rank=rank))
        _board.place_piece_at_position(King(owner), Position(file='e', rank=rank))
        _board.place_piece_at_position(Bishop(owner), Position(file='f', rank=rank))
        _board.place_piece_at_position(Knight(owner), Position(file='g', rank=rank))
        _board.place_piece_at_position(Rook(owner), Position(file='h', rank=rank))

    def place_pawns_in_rank(_board: ChessBoard, rank: int, owner: PieceColor):
        for file in 'abcdefgh':
            _board.place_piece_at_position(Pawn(owner), Position(file=file, rank=rank))

    board = ChessBoard()
    place_pieces_in_rank(board, 8, PieceColor.BLACK)
    place_pawns_in_rank(board, 7, PieceColor.BLACK)
    place_pawns_in_rank(board, 2, PieceColor.WHITE)
    place_pieces_in_rank(board, 1, PieceColor.WHITE)

    return board


def main():
    board = create_board()
    board.move_piece_at_position_to_position(Position(position='e2'), Position(position='e4'))
    board.move_piece_at_position_to_position(Position(position='e7'), Position(position='e5'))
    board.print()


if __name__ == '__main__':
    main()
