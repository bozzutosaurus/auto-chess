import enum
import typing
from position import Position


class PieceColor(enum.Enum):
    WHITE = enum.auto()
    BLACK = enum.auto()


class PieceType(enum.Enum):
    KING = enum.auto()
    QUEEN = enum.auto()
    ROOK = enum.auto()
    BISHOP = enum.auto()
    KNIGHT = enum.auto()
    PAWN = enum.auto()


class Piece:
    def __init__(self, piece_type: PieceType, symbol: str, owner: PieceColor):
        self._type = piece_type
        self._symbol = symbol
        self._owner = owner
        self._position = None

    @property
    def owner(self) -> PieceColor:
        return self._owner

    @property
    def symbol(self) -> str:
        return f"{'w' if self._owner is PieceColor.WHITE else 'b'}{self._symbol}"

    @property
    def position(self) -> Position:
        return self._position

    @position.setter
    def position(self, position):
        self._position = position

    def _sequential_moves(self, board: 'ChessBoard', rank_mod: int, file_mod: int,
                          max_moves: typing.Optional[int] = None) -> typing.Set[Position]:
        available_moves = set()
        offset = 1
        while True:
            target_position = Position.make(rank=self.position.rank + (offset * rank_mod),
                                            file=chr(ord(self.position.file) + (offset * file_mod)))
            if not target_position:
                break  # reached edge of board

            target_piece = board.get_piece_at_position(target_position)
            if target_piece:
                if target_piece.owner == self.owner:
                    break  # cannot capture or proceed pass own piece
                else:
                    available_moves.add(target_position)
                    break  # can capture but cannot move past position

            available_moves.add(target_position)
            offset += 1

            if max_moves and len(available_moves) == max_moves:
                break  # limited movement pieces (think pawn, king)

        return available_moves
