import typing

from pieces.piece import PieceColor, PieceType, Piece
from position import Position


class Knight(Piece):
    def __init__(self, owner: PieceColor):
        super().__init__(PieceType.KNIGHT, 'N', owner)

    def _move_with_offset(self, board: 'ChessBoard', rank_offset: int, file_offset: int) -> typing.Optional[Position]:

        target_position = Position.make(rank=self.position.rank + rank_offset,
                                        file=chr(ord(self.position.file) + file_offset))
        if not target_position:
            return None

        target_piece = board.get_piece_at_position(target_position)
        if target_piece and target_piece.owner == self.owner:
            return None

        return target_position

    def available_positions_on_board(self, board: 'ChessBoard') -> typing.Set[Position]:
        available_positions = set()
        for position in [self._move_with_offset(board, -1, -2), self._move_with_offset(board, -1, 2),
                         self._move_with_offset(board, 1, -2), self._move_with_offset(board, 1, 2),
                         self._move_with_offset(board, -2, -1), self._move_with_offset(board, -2, 1),
                         self._move_with_offset(board, 2, -1), self._move_with_offset(board, 2, 1)]:
            if position:
                available_positions.add(position)
        return available_positions
