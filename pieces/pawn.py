import typing

from pieces.piece import PieceColor, PieceType, Piece
from position import Position


class Pawn(Piece):
    def __init__(self, owner: PieceColor):
        super().__init__(PieceType.PAWN, 'P', owner)

    def is_in_starting_position(self):
        if self.owner == PieceColor.WHITE and self.position.rank == 2:
            return True

        if self.owner == PieceColor.BLACK and self.position.rank == 7:
            return True

        return False

    def move_forward_one_square(self, board: 'ChessBoard') -> typing.Optional[Position]:
        if self.owner == PieceColor.WHITE:
            target_position = Position.make(rank=self.position.rank + 1, file=self.position.file)
            if target_position and not board.get_piece_at_position(target_position):
                return target_position

        if self.owner == PieceColor.BLACK:
            target_position = Position.make(rank=self.position.rank - 1, file=self.position.file)
            if target_position and not board.get_piece_at_position(target_position):
                return target_position

        return None

    def move_forward_two_squares(self, board: 'ChessBoard') -> typing.Optional[Position]:
        if self.owner == PieceColor.WHITE:
            target_position = Position.make(rank=self.position.rank + 2, file=self.position.file)
            intermediate_position = Position.make(rank=self.position.rank + 1, file=self.position.file)
            if all([self.is_in_starting_position(), target_position, not board.get_piece_at_position(target_position),
                    not board.get_piece_at_position(intermediate_position)]):
                return target_position

        if self.owner == PieceColor.BLACK:
            target_position = Position(rank=self.position.rank - 2, file=self.position.file)
            intermediate_position = Position.make(rank=self.position.rank - 1, file=self.position.file)
            if all([self.is_in_starting_position(), target_position, not board.get_piece_at_position(target_position),
                    not board.get_piece_at_position(intermediate_position)]):
                return target_position

        return None

    def _move_diagnoally_one_square(self, board: 'ChessBoard', offset: int) -> typing.Optional[Position]:
        if self.owner == PieceColor.WHITE:
            target_position = Position.make(rank=self.position.rank + 1, file=chr(ord(self.position.file) + offset))
            if not target_position:
                return None

            target_piece = board.get_piece_at_position(target_position)
            if target_piece and target_piece.owner == PieceColor.BLACK:
                return target_position

        if self.owner == PieceColor.BLACK:
            target_position = Position.make(rank=self.position.rank - 1, file=chr(ord(self.position.file) + offset))
            if not target_position:
                return None

            target_piece = board.get_piece_at_position(target_position)
            if target_piece and target_piece.owner == PieceColor.WHITE:
                return target_position

        return None

    def move_diagnoally_left_one_square(self, board: 'ChessBoard') -> typing.Optional[Position]:
        return self._move_diagnoally_one_square(board, -1)

    def move_diagnoally_right_one_square(self, board: 'ChessBoard') -> typing.Optional[Position]:
        return self._move_diagnoally_one_square(board, 1)

    def available_positions_on_board(self, board: 'ChessBoard') -> typing.Set[Position]:
        available_positions = set()
        for position in [self.move_forward_one_square(board), self.move_forward_two_squares(board),
                         self.move_diagnoally_left_one_square(board), self.move_diagnoally_right_one_square(board)]:
            if position:
                available_positions.add(position)
        return available_positions



