import typing

from pieces.piece import PieceColor, PieceType, Piece
from position import Position


class Bishop(Piece):
    def __init__(self, owner: PieceColor):
        super().__init__(PieceType.BISHOP, 'B', owner)

    def _northwest_moves(self, board: 'ChessBoard') -> typing.Set[Position]:
        return self._sequential_moves(board, 1, -1)

    def _northeast_moves(self, board: 'ChessBoard') -> typing.Set[Position]:
        return self._sequential_moves(board, 1, 1)

    def _southwest_moves(self, board: 'ChessBoard') -> typing.Set[Position]:
        return self._sequential_moves(board, -1, -1)

    def _southeast_moves(self, board: 'ChessBoard') -> typing.Set[Position]:
        return self._sequential_moves(board, -1, 1)

    def available_positions_on_board(self, board: 'ChessBoard') -> typing.Set[Position]:
        available_moves = set()
        available_moves = available_moves.union(self._northwest_moves(board))
        available_moves = available_moves.union(self._northeast_moves(board))
        available_moves = available_moves.union(self._southwest_moves(board))
        available_moves = available_moves.union(self._southeast_moves(board))
        return available_moves
